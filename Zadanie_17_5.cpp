﻿#include <iostream>
#include <math.h>

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector() : x(3), y(3), z(3)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Length()
	{
		std::cout << "\n" << "length vector = " << sqrt(x * x + y * y + z * z);
	}
	
};

int main()
{
	Vector v;
	v.Length();
}